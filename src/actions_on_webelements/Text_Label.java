package actions_on_webelements;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Text_Label {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "D:\\Selenium Batch 48\\drivers\\chromedriver_win32\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.facebook.com/");
		
		WebElement createAnAccount = driver.findElement(By.xpath("//span[text()='Create an account']"));
		System.out.println("Create An Account text is Displayed: "+createAnAccount.isDisplayed());
		
		String text = createAnAccount.getText();
		System.out.println(text);
		
		System.out.println(createAnAccount.getCssValue("font-size"));

		Thread.sleep(3000);
		driver.quit();
	}

}
