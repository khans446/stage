package actions_on_webelements;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class DropDown2 {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "D:\\Selenium Batch 48\\drivers\\chromedriver_win32\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.facebook.com/");
		driver.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);
		WebElement year = driver.findElement(By.id("year"));

		Select sel = new Select(year);
		
		//sel.selectByIndex(1);
		//sel.selectByVisibleText("2020");
		sel.selectByValue("2020");
		Thread.sleep(4000);
		/*
		WebElement selectedOption = sel.getFirstSelectedOption();
		System.out.println(selectedOption);
		System.out.println(selectedOption.getText());
		Thread.sleep(6000);
		*/
		/*
		List<WebElement> allOptions = sel.getOptions();
		System.out.println("Total no.of Options available: "+allOptions.size());
		for(int i=0;i<allOptions.size();i++) {
			
			System.out.println(allOptions.get(i).getText());
		}
		*/
		
		
		System.out.println("It's a Multi Dropdown: "+sel.isMultiple());
		
		List<WebElement> allSelectedOptions = sel.getAllSelectedOptions();
		System.out.println("Total no.of Options available: "+allSelectedOptions.size());
		for(int i=0;i<allSelectedOptions.size();i++) {
			
			System.out.println(allSelectedOptions.get(i).getText());
		}
		driver.quit();
	}

}
