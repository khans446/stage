package actions_on_webelements;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Images {

	public static void main(String[] args) {
		
		System.setProperty("webdriver.chrome.driver", "D:\\Selenium Batch 48\\drivers\\chromedriver_win32\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.facebook.com/");
		
		WebElement img = driver.findElement(By.xpath("//div[contains(text(),'Facebook helps')]/following::img[1]"));
		System.out.println("Image is displayed: "+img.isDisplayed());
		
		System.out.println("Size of the image is :"+img.getSize());
		
		
	}

}
