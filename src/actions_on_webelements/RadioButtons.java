package actions_on_webelements;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class RadioButtons {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "D:\\Selenium Batch 48\\drivers\\chromedriver_win32\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.facebook.com/");
		
		WebElement maleRadioBtn = driver.findElement(By.cssSelector("#u_0_7"));
		System.out.println("Male Radio button is Selected: "+maleRadioBtn.isSelected());
		maleRadioBtn.click();
		Thread.sleep(3000);
		
		System.out.println("After clicking, Male Radio button is Selected: "+maleRadioBtn.isSelected());
		Thread.sleep(3000);
		driver.quit();
	}

}
