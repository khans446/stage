package actions_on_webelements;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Checkboxes {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "D:\\Selenium Batch 48\\drivers\\chromedriver_win32\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://aprtacitizen.epragathi.org/#!/dlmodule");

		WebElement changeOfAddress = driver.findElement(By.xpath("//div[@class='row dlservices']/div[2]/label"));
		System.out.println("Change of Address before Selecting: "+changeOfAddress.isSelected());

		changeOfAddress.click();

		Thread.sleep(6000);

		System.out.println("After clicking, Change Of Address is Selected: "+changeOfAddress.isSelected());
		Thread.sleep(3000);
		driver.quit();


	}

}
