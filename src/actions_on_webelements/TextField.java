package actions_on_webelements;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

public class TextField {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "D:\\Selenium Batch 48\\drivers\\chromedriver_win32\\chromedriver.exe");
		WebDriver driver = new InternetExplorerDriver();
		driver.manage().window().maximize();
		driver.get("https://www.facebook.com/");
		
		WebElement emailOrPhone = driver.findElement(By.id("email"));
		emailOrPhone.sendKeys("Selenium");
		Thread.sleep(2000);
		emailOrPhone.clear();
		emailOrPhone.sendKeys("SeleniumBatch48");
		Thread.sleep(2000);
		
		String textEntered = emailOrPhone.getAttribute("value");
		System.out.println(textEntered);
		driver.quit();
	}

}
