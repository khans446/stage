package actions_on_webelements;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class DropDown1 {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "D:\\Selenium Batch 48\\drivers\\chromedriver_win32\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://semantic-ui.com/modules/dropdown.html");
		JavascriptExecutor jsx = (JavascriptExecutor)driver;
		jsx.executeScript("window.scrollBy(0,300)", "");
		
		WebElement gender = driver.findElement(By.xpath("//div[contains(@class,'ui selection dropdown')]/i[@class='dropdown icon']"));
		gender.click();
		Thread.sleep(4000);
		
		WebElement female = driver.findElement(By.xpath("//div[contains(@class,'ui selection dropdown')]/div/div[text()='Female']"));
		female.click();
		Thread.sleep(6000);
		driver.quit();
	}
	

}
