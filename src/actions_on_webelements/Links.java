package actions_on_webelements;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Links {

	public static void main(String[] args) throws InterruptedException {
		
		System.setProperty("webdriver.chrome.driver", "D:\\Selenium Batch 48\\drivers\\chromedriver_win32\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.facebook.com/");
		
		//WebElement dataPolicy = driver.findElement(By.linkText("Data Policy"));
		WebElement dataPolicy = driver.findElement(By.partialLinkText("Data"));
		dataPolicy.click();
		
		Thread.sleep(3000);
		
		driver.close();


	}

}
