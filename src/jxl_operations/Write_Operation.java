package jxl_operations;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import org.testng.annotations.Test;

import jxl.Workbook;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

public class Write_Operation {

	@Test
	public void writeData() throws IOException, RowsExceededException, WriteException {
		
		
		FileOutputStream fos = new FileOutputStream("D:\\ExcelFiles\\Batch48.xls");
		
		WritableWorkbook wb = Workbook.createWorkbook(fos);
		
		WritableSheet s = wb.createSheet("Names", 0);
		
		Label l = new Label(0, 0, "Dinesh");
		s.addCell(l);
		
		l = new Label(0, 1, "Priyanka");
		s.addCell(l);
		
		l = new Label(0, 2, "Pooja");
		s.addCell(l);
		
		l = new Label(0, 3, "Rajini");
		s.addCell(l);
		
		l = new Label(0, 4, "Sunu");
		s.addCell(l);
		
		wb.write();
		
		wb.close();
		
	}
}
