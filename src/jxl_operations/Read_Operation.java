package jxl_operations;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;
import utilities_demo.Base;

public class Read_Operation extends Base{

	@Test
	public void testLogin() throws InterruptedException, BiffException, IOException {

		FileInputStream fis = new FileInputStream("D:\\ExcelFiles\\LoginData.xls");

		Workbook wb = Workbook.getWorkbook(fis);

		Sheet s = wb.getSheet("Login_Data");

		System.out.println("Total no.of Rows "+s.getRows());

		System.out.println("Total no.of Rows "+s.getColumns());


		String userName = s.getCell(0,1).getContents();		
		String password = s.getCell(1,1).getContents();
		
		
		WebElement emailOrPhone = driver.findElement(By.id("email"));

		WebElement pwd = driver.findElement(By.id("pass"));
		
		emailOrPhone.sendKeys(userName);

		pwd.sendKeys(password);

		driver.findElement(By.xpath("//input[@value='Log In']")).click();
		Thread.sleep(6000);
	
	}
}
