package keyboard_events;

import java.awt.RenderingHints.Key;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import utilities_demo.Common;

public class KeyboardEvents extends Common{

	
	// https://www.facebook.com/
	
	
	@Test
	public void testDemo() throws InterruptedException {
		
		WebElement email = driver.findElement(By.name("email"));
		//email.sendKeys("SELENIUM");
		Actions action = new Actions(driver);
		
		action.keyDown(Keys.SHIFT).sendKeys(email, "selenium").keyUp(Keys.SHIFT).build().perform();
		
		action.keyDown(email, Keys.SHIFT).sendKeys("selenium").keyUp(Keys.SHIFT).build().perform();
		
		action.sendKeys(Keys.F1).build().perform();
		
		//action.sendKeys(month, Keys.DOWN).sendKeys(Keys.DOWN).sendKeys(Keys.ENTER).build().perform();
		//Thread.sleep(5000);
	}
}
