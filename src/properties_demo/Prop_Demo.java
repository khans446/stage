package properties_demo;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import jxl.read.biff.BiffException;
import utilities_demo.Base;
import utilities_demo.PropertiesRead;

public class Prop_Demo extends Base{
	
	@Test
	public void testLogin() throws InterruptedException, BiffException, IOException {

		/*
		FileInputStream fis = new FileInputStream("D:\\Selenium Batch 48\\workspace\\SeleniumBatch48\\Execution.properties");

		Properties prop = new Properties();
		
		prop.load(fis);
		
		String userName = prop.getProperty("UserName");
		String password = prop.getProperty("Password");
		*/
		
		String userName = PropertiesRead.readData("D:\\Selenium Batch 48\\workspace\\SeleniumBatch48\\Execution.properties", "UserName");
		
		String password = PropertiesRead.readData("D:\\Selenium Batch 48\\workspace\\SeleniumBatch48\\Execution.properties", "Password");
		
		WebElement emailOrPhone = driver.findElement(By.id("email"));

		WebElement pwd = driver.findElement(By.id("pass"));
		
		emailOrPhone.sendKeys(userName);

		pwd.sendKeys(password);

		driver.findElement(By.xpath("//input[@value='Log In']")).click();
		Thread.sleep(6000);
	
	}

}
