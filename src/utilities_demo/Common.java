package utilities_demo;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Common {

	public WebDriver driver;
	@Before
	public void setUp() {
		System.setProperty("webdriver.chrome.driver", "D:\\Selenium Batch 48\\drivers\\chromedriver_win32\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
		//driver.get("http://demo.guru99.com/test/delete_customer.php");
		driver.manage().deleteAllCookies();
		driver.get("https://www.aptransport.org/index.html");
		//driver.get("https://jqueryui.com/droppable/");
		//driver.get("https://the-internet.herokuapp.com/nested_frames");
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		
		
	}
	
	@After
	public void tearDown() {
		driver.quit();
	}
}
