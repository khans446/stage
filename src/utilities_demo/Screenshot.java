package utilities_demo;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

public class Screenshot{
	
	public static WebDriver driver;
	
	public static void screenshot(WebDriver driver) throws IOException {
		
		SimpleDateFormat df = new SimpleDateFormat("dd_M_yyyy HH_mm_ss");
		Date date = new Date();
		String currentTime = df.format(date);

		File source = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);

		File destination = new File("D:\\Screenshots\\"+currentTime+".png");

		FileUtils.copyFile(source, destination);
		
	}

}
