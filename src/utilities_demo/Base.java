package utilities_demo;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.junit.Before;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;

import or.Login_Page;

public class Base {

	public WebDriver driver;

	public Login_Page login;
	
	@BeforeMethod
	public void setUp() throws IOException {
		System.setProperty("webdriver.chrome.driver", "D:\\Selenium Batch 48\\drivers\\chromedriver_win32\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		
		int implicitWait = PropertiesRead.readIntData("D:\\Selenium Batch 48\\workspace\\SeleniumBatch48\\src\\wait_demo\\PropertyWait.properties", "ImplicitWait");
		
		int pageLoad = PropertiesRead.readIntData("D:\\Selenium Batch 48\\workspace\\SeleniumBatch48\\src\\wait_demo\\PropertyWait.properties", "PageLoadTimeOut");
		
		driver.manage().timeouts().pageLoadTimeout(pageLoad, TimeUnit.SECONDS);
		driver.manage().deleteAllCookies();
		
		driver.get(PropertiesRead.readData("D:\\Selenium Batch 48\\workspace\\SeleniumBatch48\\Execution.properties", "url"));
		driver.manage().timeouts().implicitlyWait(implicitWait, TimeUnit.SECONDS);	
		login = new Login_Page(driver);
		
	}
	
	/*
	@Parameters("browser")
	@BeforeMethod
	public void setUp(String browser) {
		if(browser.equalsIgnoreCase("chrome")) {
			System.setProperty("webdriver.chrome.driver", "D:\\Selenium Batch 48\\drivers\\chromedriver_win32\\chromedriver.exe");
			driver = new ChromeDriver();
		}
		else if(browser.equalsIgnoreCase("firefox")) {
			System.setProperty("webdriver.gecko.driver", "D:\\Selenium Batch 48\\drivers\\chromedriver_win32\\chromedriver.exe");
			driver = new FirefoxDriver();
		}
		else if(browser.equalsIgnoreCase("ie")) {
			System.setProperty("webdriver.ie.driver", "D:\\Selenium Batch 48\\drivers\\IEDriverServer_Win32_3.5.1\\IEDriverServer.exe");
			driver = new InternetExplorerDriver();
		}
		driver.manage().window().maximize();
		driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
		driver.manage().deleteAllCookies();
		driver.get("http://newtours.demoaut.com/");		
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);		
	}
	
	*/
	
	
	@AfterMethod
	public void tearDown() {
		driver.quit();
	}
}
