package suites_demo;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import utilities_demo.Base;

public class Support extends Base{

	@Test
	public void support() throws InterruptedException {
		
		WebElement support = driver.findElement(By.linkText("SUPPORT"));
		support.click();
		Thread.sleep(2000);
		System.out.println(driver.getTitle());
	}
}
