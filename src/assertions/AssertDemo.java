package assertions;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import utilities_demo.Common;

public class AssertDemo extends Common{

	@Test
	public void testTitle() {
		/*
		String expectedTitle = "Facebook � log in or sign up";
		
		String actualTitle = driver.getTitle();
		
		if(expectedTitle.equalsIgnoreCase(actualTitle)) {
			System.out.println("Title is matching");
		}
		else {
			System.out.println("Title is not matching");			
		}*/
		
		//Assert.assertEquals("Facebook � log in or sign up", driver.getTitle());
		
		WebElement account = driver.findElement(By.xpath("//span[text()='Create an account']"));
		Assert.assertTrue(account.isDisplayed());
		Assert.assertFalse(2>5);
		System.out.println(account.getText());
	}
	
	
}
