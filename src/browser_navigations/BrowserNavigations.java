package browser_navigations;

import org.junit.Test;

import utilities_demo.Common;

public class BrowserNavigations extends Common{

	@Test
	public void testDemo() throws InterruptedException {

		Thread.sleep(3000);

		//driver.get("https://www.google.com/");

		//Thread.sleep(3000);

		driver.navigate().back();

		Thread.sleep(3000);

		driver.navigate().forward();

		Thread.sleep(3000);

		driver.navigate().refresh();
		Thread.sleep(3000);
		
		driver.navigate().to("https://www.flipkart.com/");
		
		Thread.sleep(3000);
	}
}
