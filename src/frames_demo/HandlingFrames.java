package frames_demo;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import utilities_demo.Common;

public class HandlingFrames extends Common{

	//https://the-internet.herokuapp.com/nested_frames
	
	@Test
	public void testFrame() {
//		driver.switchTo().frame("frame-top");
//		driver.switchTo().frame("frame-middle");
			
//		driver.switchTo().frame(0);
//		driver.switchTo().frame(1);
		
		
		driver.switchTo().frame(driver.findElement(By.xpath("//html/frameset/frame[1]")));
		driver.switchTo().frame(driver.findElement(By.name("frame-middle")));
		WebElement middle = driver.findElement(By.xpath("//div[text()='MIDDLE']"));
		System.out.println(middle.isDisplayed());
		System.out.println(middle.getText());
	}
}
