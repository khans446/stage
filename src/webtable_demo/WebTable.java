package webtable_demo;

import java.util.List;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import utilities_demo.Common;

public class WebTable extends Common{

	//  https://www.aptransport.org/index.html
	
	
	@Test
	public void testDemo() {
		
		WebElement userCharges = driver.findElement(By.xpath("//span[text()='User Charges']"));
		userCharges.click();
		
		
		List<WebElement> rows = driver.findElements(By.xpath("//h1[text()='USER CHARGES']/following::table/tbody/tr"));
		System.out.println("Total no.of Rows : "+rows.size());
		
		for(int row = 0;row<rows.size();row++) {
			
			List<WebElement> row_column = rows.get(row).findElements(By.tagName("td"));
			
			for(int column = 0; column<row_column.size();column++) {
				
				System.out.print(row_column.get(column).getText());
				System.out.print("  ");
			}System.out.println();
		}
	}
}
