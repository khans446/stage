package windows_demo;

import java.util.ArrayList;
import java.util.Set;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import utilities_demo.Common;

public class HandlingWindows extends Common{

	//https://www.facebook.com/

	@Test
	public void testLogin() throws InterruptedException {

		WebElement terms = driver.findElement(By.linkText("Terms"));
		terms.click();

		Thread.sleep(3000);

		WebElement dataPolicy = driver.findElement(By.linkText("Data Policy"));
		dataPolicy.click();

		Thread.sleep(3000);

		WebElement cookiePolicy = driver.findElement(By.linkText("Cookie Policy"));
		cookiePolicy.click();
		
		System.out.println("Title of the page is: "+driver.getTitle());
		/*
		System.out.println("Title of the page is: "+driver.getTitle());

		System.out.println("Handle of the window is :"+driver.getWindowHandle());

		//driver.switchTo().window("3A7FE23109AF480A5913B3267C5BA065");
		*/
		Set<String> handles = driver.getWindowHandles();
		ArrayList<String> allHandles = new ArrayList<String>(handles);
		
		String loginPage = allHandles.get(0);
		String cookiePolicyPage = allHandles.get(1);
		String dataPolicyPage = allHandles.get(2);
		String termsPage = allHandles.get(3);
		
		driver.switchTo().window(termsPage);
		System.out.println("Title of the page is: "+driver.getTitle());
		Thread.sleep(3000);
		driver.switchTo().window(dataPolicyPage);
		System.out.println("Title of the page is: "+driver.getTitle());
		Thread.sleep(3000);
		driver.switchTo().window(cookiePolicyPage);
		System.out.println("Title of the page is: "+driver.getTitle());
	}
}
