package alerts_demo;

import org.junit.Test;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.server.handler.SubmitElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import utilities_demo.Common;

public class JavascriptAlert extends Common{

	@Test
	public void alertDemo() throws InterruptedException {
		
		WebElement submitBtn = driver.findElement(By.cssSelector("input[name='submit']"));
		submitBtn.click();
		
		Alert al = driver.switchTo().alert();
		String alertText = al.getText();
		System.out.println("The text displayed in the alert window is: "+alertText);
		Thread.sleep(4000);
		al.accept();
		
		WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.alertIsPresent());
		
		System.out.println("The text displayed in the alert window is: "+al.getText());
		Thread.sleep(4000);
		al.accept();
		Thread.sleep(4000);
	}
	
}
