package sapmle;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class LaunchChromeBrowser {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "D:\\Selenium Batch 48\\drivers\\chromedriver_win32\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.facebook.com/");
		String title = driver.getTitle();
		
		System.out.println("Title of web page is "+title);
		System.out.println("Title of web page is "+driver.getTitle());
		
		System.out.println(driver.getCurrentUrl());
		
		Thread.sleep(3000);
		
		driver.close();
		
	}
	
	

}
