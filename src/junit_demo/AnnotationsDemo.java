package junit_demo;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class AnnotationsDemo {
	
	@BeforeClass
	public static void beforeClass() {
		
		System.out.println("@BeforeClass");
	}

	@Before
	public void before() {
		
		System.out.println("@Before");
	}
	
	@Test
	public void testM1() {
		System.out.println("testM1()");
	}
	
	
	@Test
	public void testA1() {
		System.out.println("testA1");
	}
	
	@Test
	public void testB1() {
		System.out.println("testB1()");
	}
	
	@After
	public void after() {
		System.out.println("@After");
	}
	
	@AfterClass
	public static void afterClass() {
		System.out.println("@AfterClass");
	}
}
