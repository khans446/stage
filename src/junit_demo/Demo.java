package junit_demo;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Demo {

	WebDriver driver;
	@Before
	public void setUp() {
		System.setProperty("webdriver.chrome.driver", "D:\\Selenium Batch 48\\drivers\\chromedriver_win32\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
		driver.get("https://www.facebook.com/");
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}
	
	@Test
	public void testForgottenAccount() throws InterruptedException {
			
		WebElement account = driver.findElement(By.linkText("Forgotten account?"));
		account.click();
		Thread.sleep(3000);		

	}
	
	
	@Test
	public void testTerms() throws InterruptedException {
			
		WebElement terms = driver.findElement(By.linkText("Terms"));
		terms.click();
		Thread.sleep(3000);
	}
	
	@Test
	public void testLogin() throws InterruptedException {
			
		WebElement emailOrPhone = driver.findElement(By.id("email"));
		emailOrPhone.sendKeys("SeleniumBatch48");
		WebElement pwd = driver.findElement(By.id("pass"));
		pwd.sendKeys("123456");
		
		driver.findElement(By.xpath("//input[@value='Log In']")).click();
		Thread.sleep(6000);
		
	}
	@After
	public void tearDown() {
		driver.quit();
	}

}
