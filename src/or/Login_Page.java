package or;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Login_Page {

	public WebDriver driver;
	
	//WebElement  email = driver.findElement(By.id("email"));
	
	@FindBy(id = "email") public WebElement emailOrPhone;
	@FindBy(id = "pass") public WebElement password;
	@FindBy(xpath = "//input[@value='Log In']") public WebElement logIn;
	@FindBy(linkText = "Forgotten account?") public WebElement forgottenAccount;
	
	public Login_Page(WebDriver driver) {
		
		PageFactory.initElements(driver, this);
		
	}
	
	
}
