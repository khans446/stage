package poi_operations;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;


import utilities_demo.Base;

public class Read_Operatrion extends Base{
	
	@Test
	public void testLogin() throws IOException, InterruptedException {

		FileInputStream fis = new FileInputStream("D:\\ExcelFiles\\New folder\\Login_data.xlsx");

		XSSFWorkbook wb = new XSSFWorkbook(fis);
		
		XSSFSheet s = wb.getSheetAt(0);

		int rows = s.getLastRowNum();

		System.out.println("Total no.of Rows "+rows);

		//System.out.println("Total no.of Rows "+s.getColumns());
		
		

		String userName = s.getRow(1).getCell(0).getStringCellValue();

		String password = s.getRow(1).getCell(1).getStringCellValue();
		
		WebElement emailOrPhone = driver.findElement(By.id("email"));

		WebElement pwd = driver.findElement(By.id("pass"));
		
		emailOrPhone.sendKeys(userName);

		pwd.sendKeys(password);

		driver.findElement(By.xpath("//input[@value='Log In']")).click();
		Thread.sleep(6000);
		wb.close();
	}

}
