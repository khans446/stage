package testng_demo;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class Priority_Demo {

	@BeforeMethod
	public void beforeMethod() {
		System.out.println("BeforeMethod");
	}
	
	@Test(priority = 5)
	public void a1() {
		System.out.println("Test A1");
	}
	@Test(priority = 2)
	public void a2() {
		System.out.println("Test A2");
	}
	@Test(priority = 3)
	public void a5() {
		System.out.println("Test A5");
	}
	@Test(priority = 4)
	public void a4() {
		System.out.println("Test A4");
	}
	@Test(priority = 1)
	public void a3() {
		System.out.println("Test A3");
	}

	@AfterMethod
	public void afterMethod() {
		System.out.println("AfterMethod");
	}
}
