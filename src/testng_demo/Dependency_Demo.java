package testng_demo;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class Dependency_Demo {

	@BeforeMethod
	public void beforeMethod() {
		System.out.println("BeforeMethod");
	}
	
	@Test
	public void a1() {
		//System.out.println(2/0);
		System.out.println("Test A1");
		
	}
	@Test(dependsOnMethods = "a1")
	public void a2() {
		
		System.out.println("Test A2");
	}
	
	@Test(dependsOnMethods = "a1")
	public void a3() {
		
		System.out.println("Test A1");
	}
	@Test(dependsOnMethods = "a1")
	public void a4() {
		
		System.out.println("Test A2");
	}
	
	@AfterMethod
	public void afterMethod() {
		System.out.println("AfterMethod");
	}
}
