package testng_demo;

import org.testng.annotations.Test;

public class Group_Test {

	@Test(groups = "High")
	public void a1() {
		
		System.out.println("Test A1");
		
	}
	
	@Test
	public void a2() {
		
		System.out.println("Test A2");
	}
	
	@Test(groups = "H")
	public void a3() {
		
		System.out.println("Test A3");
	}
	
	@Test(groups = "M")
	public void a4() {
		
		System.out.println("Test A4");
	}
	
	@Test(groups = "High")
	public void a5() {
		
		System.out.println("Test A5");
	}
	
	@Test(groups = "Medium")
	public void a6() {
		
		System.out.println("Test A6");
	}
	
	@Test(groups = "Low")
	public void a7() {
		
		System.out.println("Test A7");
	}
}
