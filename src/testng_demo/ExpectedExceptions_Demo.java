package testng_demo;

import org.testng.annotations.Test;

public class ExpectedExceptions_Demo {

	@Test(expectedExceptions = ArithmeticException.class)
	public void a1() {
		
		System.out.println(2/0);
		System.out.println("Test A1");
		
	}
}
