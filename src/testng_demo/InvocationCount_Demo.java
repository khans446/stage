package testng_demo;

import org.testng.annotations.Test;

public class InvocationCount_Demo {

	@Test
	public void a1() {
		//System.out.println(2/0);
		System.out.println("Test A1");
		
	}
	@Test(invocationCount = 5)
	public void a2() {
		
		System.out.println("Test A2");
	}
	
	@Test
	public void a3() {
		
		System.out.println("Test A3");
	}
}
