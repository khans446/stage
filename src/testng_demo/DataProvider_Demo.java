package testng_demo;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import utilities_demo.Base;

public class DataProvider_Demo extends Base{

	
	@Test(dataProvider = "testData")
	public void login(String userName, String password) throws InterruptedException {
		
		WebElement emailOrPhone = driver.findElement(By.id("email"));
		emailOrPhone.sendKeys(userName);
		WebElement pwd = driver.findElement(By.id("pass"));
		pwd.sendKeys(password);
		
		driver.findElement(By.xpath("//input[@value='Log In']")).click();
		Thread.sleep(6000);
		
	}
	
	@DataProvider(name="testData")
	public Object[][] testData(){
	/*	
		Object[][] loginData = new Object[5][4];
		loginData[0][0] = "SeleniumBatch48";
		loginData[0][1] = "123456";
		
		loginData[1][0] = "Java";
		loginData[1][1] = "789456";
		
		loginData[2][0] = "Python";
		loginData[2][1] = "321654";
		
		loginData[3][0] = "Ruby";
		loginData[3][1] = "987654";
		
		loginData[4][0] = "Ruby";
		loginData[4][1] = "987654";
		
		return loginData;
		*/
		Object[][] loginData = {{"SeleniumBatch48","123456"},{"Java","789456"}};
		return loginData;
	}
}
