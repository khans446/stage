package testng_demo;

import org.testng.annotations.Test;

public class Skip_Test {

	@Test
	public void a1() {
		
		System.out.println("Test A1");
		
	}
	
	@Test(enabled = false)
	public void a2() {
		
		System.out.println("Test A2");
	}
	
	@Test
	public void a3() {
		
		System.out.println("Test A3");
	}
}
